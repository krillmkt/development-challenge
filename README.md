# **Development Challenge**

## **Description**
In order to evaluate your technical skills, you'll be required to turn two static images into functioning website pages.

## **Contents**
- `/wp-content/` - Contains the minimum required files/folders for your deliverables.
- `master.xd` - Adobe XD source file for both templates. It'll come in handy when you need visual hints.
- `template_1.jpg` and `template_2.jpg` - Templates you'll be creating.

## **Musts**
- Both pages will **accurately** resemble their reference
- You will determine how they'll adapt to smaller screens
- You will use Sass to programmatically create your stylesheets
- Both pages will be accessible through their unique URLs *(e.g.: https://my.project/template-1 and https://my.project/template-2)*. This means **no .html files will be allowed**, and some level of WordPress integration is required

## **Nice to haves**
- **Browser Compatibility:** Pages will be correctly displayed in the following browsers:
    - Chrome *(last 3 versions)*
    - Firefox *(last 3 versions)*
    - Safari *(last 3 versions)*
    - Opera *(last 3 versions)*
    - Edge *(last 3 versions)*
    - Internet Explorer *(10+)*
- **Performance:** Pages will be optimized based on [Lighthouse](https://developers.google.com/web/tools/lighthouse/) audits
- **SEO optimization:** You will use markup to reinforce semantics and create an appropriate [Document Outline](http://html5doctor.com/outlines/), and [Structured Data](https://developers.google.com/search/docs/guides/intro-structured-data) to provide explicit clues about the meaning of these pages to Search Engines
- **Full WordPress integration:** Pages will be completely editable and iterable using the WordPress admin.


## **Deliverables**
Direct-access repository link where we'll find (at least):
- `/wp-content/` folder containing all needed theme files and plugins
- Database dump *(.sql or gzipped)*
- Proper documentation/notes


## **Misc**
- You can use the following stylesheet to manage fonts `https://use.typekit.net/xcg2tpw.css`
- You'll need an Adobe CC subscription to use Adobe Fonts and open the .xd file
- You can use as many libraries/frameworks as you want
- You can use as many plugins as you want


